package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import java.util.Optional;

public class Pawn extends Piece {

    private static final String BLACK_PAWN_SYMBOL = "P";
    private static final String WHITE_PAWN_SYMBOL = "p";
    private final int initialRow;

    public Pawn(boolean isWhite, int initialRow) {
        super(isWhite);
        this.initialRow = initialRow;
    }

    @Override
    protected String getSymbolForBlack() {
        return BLACK_PAWN_SYMBOL;
    }

    @Override
    protected String getSymbolForWhite() {
        return WHITE_PAWN_SYMBOL;
    }

    @Override
    public boolean isMoveValid(Coordinate to, Board board) {
        Optional<Piece> pieceOnDestination = board.getPieceFor(to);
        if (pieceOnDestination.isPresent()) {
            return isEnemy(pieceOnDestination.get()) && isMovingObliquelyOneCellForward(to);
        } else {
            return isMovingOneRowForward(to) ||
                    (isMovingTwoRowsForwardFromInitialPosition(to) && hasNoObstacle(to, board));
        }
    }

    private boolean isMovingObliquelyOneCellForward(Coordinate to) {
        Coordinate from = getCurrentCoordinate();
        return to.getRow() == getRowForward(from.getRow(), 1) &&
                isMovingObliquely(from.getColumn(), to.getColumn());
    }

    private boolean isMovingObliquely(int fromColumn, int toColumn) {
        return Math.abs(fromColumn - toColumn) == 1;
    }

    private boolean hasNoObstacle(Coordinate to, Board board) {
        return board.isCellEmpty(new Coordinate(to.getColumn(), getRowBackward(to.getRow(), 1)));
    }

    private int getRowBackward(int row, int steps) {
        if (isWhite()){
            return row + steps;
        } else {
            return row - steps;
        }
    }

    private int getRowForward(int row, int steps) {
        if (isWhite()){
            return row - steps;
        } else {
            return row + steps;
        }
    }

    private boolean isMovingTwoRowsForwardFromInitialPosition(Coordinate to) {
        Coordinate from = getCurrentCoordinate();
        return isPositionInitial()
                && isColumnsSame(from, to)
                && to.getRow() == getRowForward(from.getRow(), 2);
    }

    private boolean isPositionInitial() {
        return getCurrentCoordinate().getRow() == initialRow;
    }

    private boolean isMovingOneRowForward(Coordinate to) {
        Coordinate from = getCurrentCoordinate();
        return isColumnsSame(from, to)
                && to.getRow() == getRowForward(from.getRow(), 1);
    }

    private boolean isColumnsSame(Coordinate from, Coordinate to) {
        return from.getColumn() == to.getColumn();
    }
}
