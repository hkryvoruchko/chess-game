package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

public class Bishop extends Piece {

    private static final String BLACK_BISHOP_SYMBOL = "B";
    private static final String WHITE_BISHOP_SYMBOL = "b";

    public Bishop(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isMoveValid(Coordinate to, Board board) {
        Coordinate from = getCurrentCoordinate();
        return (isMovingObliquely(from, to) && obliqueMoveHasNoObstacle(from, to, board)
                && pieceOnDestinationIsAbsentOrEnemy(board, to));
    }

    private boolean isMovingObliquely(Coordinate from, Coordinate to) {
        return Math.abs(to.getColumn() - from.getColumn()) == Math.abs(to.getRow() - from.getRow());
    }

    @Override
    String getSymbolForBlack() {
        return BLACK_BISHOP_SYMBOL;
    }

    @Override
    String getSymbolForWhite() {
        return WHITE_BISHOP_SYMBOL;
    }
}
