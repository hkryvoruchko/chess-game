package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import java.io.PrintStream;
import java.util.Optional;

import static com.hkryvoruchko.game.chess.util.IntRangeStreamUtil.getRangeArray;
import static com.hkryvoruchko.game.chess.util.IntRangeStreamUtil.getRangeStream;

public abstract class Piece {

    private Coordinate currentCoordinate;
    private boolean isWhite;

    public Piece(boolean isWhite) {
        this.isWhite = isWhite;
    }

    public boolean isWhite() {
        return isWhite;
    }

    public Coordinate getCurrentCoordinate() {
        return currentCoordinate;
    }

    public void setCurrentCoordinate(Coordinate currentCoordinate) {
        this.currentCoordinate = currentCoordinate;
    }

    public abstract boolean isMoveValid(Coordinate to, Board board);

    public void draw(PrintStream output) {
        if (isWhite){
            output.print(getSymbolForWhite());
        }
        else{
            output.print(getSymbolForBlack());
        }
    }

    protected boolean straightMoveHasNoObstacle(Coordinate from, Coordinate to, Board board) {
        if (from.getColumn() == to.getColumn()) {
            return columnHasNoObstacle(from, to, board);
        } else {
            return rowHasNoObstacle(from, to, board);
        }
    }

    private boolean rowHasNoObstacle(Coordinate from, Coordinate to, Board board) {
        return getRangeStream(from.getColumn(), to.getColumn())
                .noneMatch(col -> board.isCellNotEmpty(new Coordinate(col, from.getRow())));
    }

    private boolean columnHasNoObstacle(Coordinate from, Coordinate to, Board board) {
        return getRangeStream(from.getRow(), to.getRow())
                .noneMatch(row -> board.isCellNotEmpty(new Coordinate(from.getColumn(), row)));
    }

    protected boolean obliqueMoveHasNoObstacle(Coordinate from, Coordinate to, Board board) {
        int[] columnIndexes = getRangeArray(from.getColumn(), to.getColumn());
        int[] rowIndexes = getRangeArray(from.getRow(), to.getRow());
        for (int i = 0; i < columnIndexes.length; i++) {
            if (board.isCellNotEmpty(new Coordinate(columnIndexes[i], rowIndexes[i]))) {
                return false;
            }
        }
        return true;
    }

    protected boolean isEnemy(Piece piece) {
        return isWhite() != piece.isWhite();
    }

    protected boolean pieceOnDestinationIsAbsentOrEnemy(Board board, Coordinate coordinate) {
        Optional<Piece> piece = board.getPieceFor(coordinate);
        return ! piece.isPresent() || isEnemy(piece.get());
    }

    abstract String getSymbolForBlack();

    abstract String getSymbolForWhite();

}
