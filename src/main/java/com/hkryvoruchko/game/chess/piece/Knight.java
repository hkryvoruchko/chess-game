package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

public class Knight extends Piece {

    private static final String BLACK_KNIGHT_SYMBOL = "N";
    private static final String WHITE_KNIGHT_SYMBOL = "n";

    public Knight(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isMoveValid(Coordinate to, Board board) {
        Coordinate from = getCurrentCoordinate();
        return (isKnightsMove(from, to) && pieceOnDestinationIsAbsentOrEnemy(board, to));
    }

    private boolean isKnightsMove(Coordinate from, Coordinate to) {
        return (Math.abs(to.getRow() - from.getRow()) == 2 && (Math.abs(to.getColumn() - from.getColumn()) == 1))
                || (Math.abs(to.getRow() - from.getRow()) == 1 && (Math.abs(to.getColumn() - from.getColumn()) == 2));
    }

    @Override
    String getSymbolForBlack() {
        return BLACK_KNIGHT_SYMBOL;
    }

    @Override
    String getSymbolForWhite() {
        return WHITE_KNIGHT_SYMBOL;
    }
}
