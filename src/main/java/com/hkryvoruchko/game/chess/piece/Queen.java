package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

public class Queen extends Piece {

    private static final String BLACK_QUEEN_SYMBOL = "Q";
    private static final String WHITE_QUEEN_SYMBOL = "q";

    public Queen(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isMoveValid(Coordinate to, Board board) {
        Coordinate from = getCurrentCoordinate();
        return (isMovingObliquelyOrStraight(from, to) && hasNoObstacles(from, to, board) && pieceOnDestinationIsAbsentOrEnemy(board, to));
    }

    private boolean hasNoObstacles(Coordinate from, Coordinate to, Board board) {
        if (isMovingStraight(from, to)) {
            return straightMoveHasNoObstacle(from, to, board);
        } else {
            return obliqueMoveHasNoObstacle(from, to, board);
        }
    }

    private boolean isMovingObliquelyOrStraight(Coordinate from, Coordinate to) {
        return isMovingObliquely(from, to) || isMovingStraight(from, to);
    }

    private boolean isMovingObliquely(Coordinate from, Coordinate to) {
        return Math.abs(to.getColumn() - from.getColumn()) == Math.abs(to.getRow() - from.getRow());
    }

    private boolean isMovingStraight(Coordinate from, Coordinate to) {
        return from.getColumn() == to.getColumn() || from.getRow() == to.getRow();
    }

    @Override
    String getSymbolForBlack() {
        return BLACK_QUEEN_SYMBOL;
    }

    @Override
    String getSymbolForWhite() {
        return WHITE_QUEEN_SYMBOL;
    }
}
