package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

public class King extends Piece {

    private static final String BLACK_KING_SYMBOL = "K";
    private static final String WHITE_KING_SYMBOL = "k";

    public King(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isMoveValid(Coordinate to, Board board) {
        Coordinate from = getCurrentCoordinate();
        return (differenceNotMoreThanOne(from.getColumn(), to.getColumn())
                && differenceNotMoreThanOne(from.getRow(), to.getRow())
                && pieceOnDestinationIsAbsentOrEnemy(board, to));
    }

    private boolean differenceNotMoreThanOne(int column, int column2) {
        return Math.abs(column - column2) <= 1;
    }

    @Override
    String getSymbolForBlack() {
        return BLACK_KING_SYMBOL;
    }

    @Override
    String getSymbolForWhite() {
        return WHITE_KING_SYMBOL;
    }
}
