package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

public class Rook extends Piece {

    private static final String BLACK_ROOK_SYMBOL = "R";
    private static final String WHITE_ROOK_SYMBOL = "r";

    public Rook(boolean isWhite) {
        super(isWhite);
    }

    @Override
    public boolean isMoveValid(Coordinate to, Board board) {
        Coordinate from = getCurrentCoordinate();
        return (isMovingStraight(from, to) && straightMoveHasNoObstacle(from, to, board)
                && pieceOnDestinationIsAbsentOrEnemy(board, to));
    }

    private boolean isMovingStraight(Coordinate from, Coordinate to) {
        return from.getColumn() == to.getColumn() || from.getRow() == to.getRow();
    }

    @Override
    String getSymbolForBlack() {
        return BLACK_ROOK_SYMBOL;
    }

    @Override
    String getSymbolForWhite() {
        return WHITE_ROOK_SYMBOL;
    }
}
