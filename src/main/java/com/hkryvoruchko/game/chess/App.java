package com.hkryvoruchko.game.chess;

public class App {

    public static void main(String[] args) {
        ChessGame chessGame = new ChessGame(System.out, System.err, System.in);
        chessGame.playGame();
    }
}
