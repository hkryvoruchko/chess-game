package com.hkryvoruchko.game.chess.input;

import com.hkryvoruchko.game.chess.coordinate.Coordinate;
import com.hkryvoruchko.game.chess.coordinate.Move;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static com.hkryvoruchko.game.chess.util.CoordinateConverter.convertColumnFromUserPresentation;
import static com.hkryvoruchko.game.chess.util.CoordinateConverter.convertRowFromUserPresentation;

/**
 * Handles and validates user input.
 */
public class UserInputController {

    private static final String VALID_INPUT_PATTERN = "[a-h][1-8] to [a-h][1-8]";

    private PrintStream output;
    private PrintStream errorOutput;
    private Scanner input;

    public UserInputController(PrintStream output, PrintStream errorOutput, InputStream input) {
        this.output = output;
        this.errorOutput = errorOutput;
        this.input = new Scanner(input);
    }

    public Move getUserMove(){
        output.println("Please enter movement value. Example: d2 to d4");
        String userInput = input.nextLine();
        if(isValid(userInput)) {
            Move move = parseMoveCoordinates(userInput);
            if(move.getFrom().equals(move.getTo())) {
                errorOutput.println("Initial point and destination point should be different");
            } else {
                return move;
            }
        } else {
            errorOutput.println("Invalid input");
        }
        return getUserMove();
    }

    private Move parseMoveCoordinates(String userInput) {
        Coordinate from = parseCoordinate(userInput.charAt(0), userInput.charAt(1));
        Coordinate to = parseCoordinate(userInput.charAt(6), userInput.charAt(7));
        return new Move(from, to);
    }

    private Coordinate parseCoordinate(char firstChar, char secondChar) {
        return new Coordinate(convertColumnFromUserPresentation(firstChar), convertRowFromUserPresentation(secondChar));
    }

    private boolean isValid(String userInput) {
        return userInput.matches(VALID_INPUT_PATTERN);
    }

}
