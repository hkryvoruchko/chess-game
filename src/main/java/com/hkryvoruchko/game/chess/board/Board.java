package com.hkryvoruchko.game.chess.board;

import com.hkryvoruchko.game.chess.coordinate.Coordinate;
import com.hkryvoruchko.game.chess.coordinate.Move;
import com.hkryvoruchko.game.chess.piece.Piece;

import java.io.PrintStream;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class Board {

    public static final int BOARD_SIZE = 8;

    //[row][column]
    private Piece[][] board = new Piece[BOARD_SIZE][BOARD_SIZE];

    public Optional<Piece> getPieceFor(Coordinate coordinate){
        return ofNullable(board[coordinate.getRow()][coordinate.getColumn()]);
    }

    public boolean isCellEmpty(Coordinate coordinate){
        return ! isCellNotEmpty(coordinate);
    }

    public boolean isCellNotEmpty(Coordinate coordinate){
        return getPieceFor(coordinate).isPresent();
    }

    public void put(Coordinate coordinate, Piece piece) {
        piece.setCurrentCoordinate(coordinate);
        board[coordinate.getRow()][coordinate.getColumn()] = piece;
    }

    public void printBoard(PrintStream output) {
        output.println("\ta\tb\tc\td\te\tf\tg\th");
        for (int row = 0; row < board.length; row++) {
            printRow(row, output);
            output.println();
        }
    }

    private void printRow(int row, PrintStream output) {
        printNumberOfCurrentRow(row, output);
        for (Piece piece : board[row]) {
            printCell(piece, output);
        }
    }

    private void printCell(Piece piece, PrintStream output) {
        if(piece != null){
            piece.draw(output);
        }
        output.print("\t");
    }

    private void printNumberOfCurrentRow(int row, PrintStream output) {
        output.print(BOARD_SIZE - row + ".\t");
    }

    public void makeMove(Move move) {
        Coordinate from = move.getFrom();
        Coordinate to = move.getTo();
        Piece pieceToMove = getPieceFor(from).get();
        pieceToMove.setCurrentCoordinate(to);
        put(to, pieceToMove);
        makeCellEmpty(from);
    }

    private void makeCellEmpty(Coordinate coordinate) {
        board[coordinate.getRow()][coordinate.getColumn()] = null;
    }
}
