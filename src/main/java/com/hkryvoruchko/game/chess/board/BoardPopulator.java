package com.hkryvoruchko.game.chess.board;

import com.hkryvoruchko.game.chess.coordinate.Coordinate;
import com.hkryvoruchko.game.chess.piece.*;

import static java.util.Arrays.stream;

/**
 * Populates board with initial set of pieces.
 */
public class BoardPopulator {

    private static final int INITIAL_ROW_FOR_WHITE_PAWNS = 6;
    private static final int INITIAL_ROW_FOR_BLACK_PAWNS = 1;
    private static final int INITIAL_ROW_FOR_REST_OF_WHITE_PIECES = 7;
    private static final int INITIAL_ROW_FOR_REST_OF_BLACK_PIECES = 0;

    private static final int[] COLUMNS_OF_ROOKS = {0, 7};
    private static final int[] COLUMNS_OF_KNIGHTS = {1, 6};
    private static final int[] COLUMNS_OF_BISHOPS = {2, 5};
    private static final int COLUMN_OF_KING = 4;
    private static final int COLUMN_OF_QUEEN = 3;

    private Board board;

    public BoardPopulator(Board board) {
        this.board = board;
    }

    public void populateInitialPieceSet() {
        populateRowWithPawns(true, INITIAL_ROW_FOR_WHITE_PAWNS);
        populateRowWithRestOfPieces(true, INITIAL_ROW_FOR_REST_OF_WHITE_PIECES);
        populateRowWithPawns(false, INITIAL_ROW_FOR_BLACK_PAWNS);
        populateRowWithRestOfPieces(false, INITIAL_ROW_FOR_REST_OF_BLACK_PIECES);
    }

    private void populateRowWithRestOfPieces(boolean isWhite, int row) {
        stream(COLUMNS_OF_ROOKS).forEach(col ->
                board.put(new Coordinate(col, row), new Rook(isWhite)));
        stream(COLUMNS_OF_KNIGHTS).forEach(col ->
                board.put(new Coordinate(col, row), new Knight(isWhite)));
        stream(COLUMNS_OF_BISHOPS).forEach(col ->
                board.put(new Coordinate(col, row), new Bishop(isWhite)));

        board.put(new Coordinate(COLUMN_OF_KING, row), new King(isWhite));
        board.put(new Coordinate(COLUMN_OF_QUEEN, row), new Queen(isWhite));
    }

    private void populateRowWithPawns(boolean isWhite, int initialRow) {
        for (int column = 0; column < Board.BOARD_SIZE; column++) {
            Coordinate coordinate = new Coordinate(column, initialRow);
            board.put(coordinate, new Pawn(isWhite, initialRow));
        }
    }

}
