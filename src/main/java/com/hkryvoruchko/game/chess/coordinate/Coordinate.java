package com.hkryvoruchko.game.chess.coordinate;

import com.hkryvoruchko.game.chess.board.Board;

import java.util.Objects;

public class Coordinate {

    private int column;
    private int row;

    public Coordinate(int column, int row) {
        check(column);
        check(row);
        this.column = column;
        this.row = row;
    }

    private void check(int coordinate) {
        if(coordinate < 0 || coordinate >= Board.BOARD_SIZE) {
            throw new IllegalArgumentException("coordinate should be within 0 and " + (Board.BOARD_SIZE - 1));
        }
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return column == that.column &&
                row == that.row;
    }

    @Override
    public int hashCode() {

        return Objects.hash(column, row);
    }
}
