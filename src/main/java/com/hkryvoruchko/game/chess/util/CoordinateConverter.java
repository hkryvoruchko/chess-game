package com.hkryvoruchko.game.chess.util;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

public class CoordinateConverter {

    private static final int SHIFT_OF_LETTER_IN_INTEGER_REPRESENTATION = 97;

    private CoordinateConverter() {
        //util class
    }

    public static int convertColumnFromUserPresentation(char column){
        return (int)column - SHIFT_OF_LETTER_IN_INTEGER_REPRESENTATION;
    }

    public static int convertRowFromUserPresentation(char row){
        return Board.BOARD_SIZE - Integer.parseInt(row + "");
    }

    public static String convertToUserPresentation(Coordinate coordinate){
        return convertColumnToUserPresentation(coordinate.getColumn()) +
                convertRowToUserPresentation(coordinate.getRow());
    }

    private static String convertColumnToUserPresentation(int column){
        return String.valueOf((char) (column + SHIFT_OF_LETTER_IN_INTEGER_REPRESENTATION));
    }

    private static String convertRowToUserPresentation(int row){
        return String.valueOf(Board.BOARD_SIZE - row);
    }



}
