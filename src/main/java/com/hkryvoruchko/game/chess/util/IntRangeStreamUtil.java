package com.hkryvoruchko.game.chess.util;

import java.util.stream.IntStream;

public class IntRangeStreamUtil {

    private IntRangeStreamUtil() {
        //util class
    }

    public static IntStream getRangeStream(int from, int to) {
        if(from > to){
            return IntStream.range(to + 1, from);
        } else {
            return IntStream.range(from + 1, to);
        }
    }

    public static int[] getRangeArray(int from, int to) {
        return getRangeStream(from, to).toArray();
    }
}
