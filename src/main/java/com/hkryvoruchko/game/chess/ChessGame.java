package com.hkryvoruchko.game.chess;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.board.BoardPopulator;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;
import com.hkryvoruchko.game.chess.coordinate.Move;
import com.hkryvoruchko.game.chess.input.UserInputController;
import com.hkryvoruchko.game.chess.piece.King;
import com.hkryvoruchko.game.chess.piece.Piece;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Optional;

import static com.hkryvoruchko.game.chess.util.CoordinateConverter.convertToUserPresentation;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class ChessGame {

    private static final String LINE_BORDER = "--------------------------------------------------------------------------";
    private static final String WHITE_PLAYER = "White";
    private static final String BLACK_PLAYER = "Black";

    private Board board = new Board();

    private boolean isWhitePlayersTurn = true;
    private boolean gameOver = false;

    private int whitePlayerScore = 0;
    private int blackPlayerScore = 0;

    private UserInputController inputController;
    private PrintStream output;
    private PrintStream errorOutput;

    public ChessGame(PrintStream output, PrintStream errorOutput, InputStream input) {
        this.output = output;
        this.errorOutput = errorOutput;
        this.inputController = new UserInputController(output, errorOutput, input);
    }

    public void playGame(){
        new BoardPopulator(board).populateInitialPieceSet();
        output.println("Upper-case letters - black pieces. Lower-case letters - white pieces.");
        while (! gameOver) {
            makeTurn();
        }
    }

    private void makeTurn() {
        board.printBoard(output);
        printScore();
        printWhoseTurn();
        Move userMove = getValidUserMove();
        takeEnemyPiece(userMove.getTo()).ifPresent(this::updateScoreOrSetGameOverIfKing);
        board.makeMove(userMove);
        changePlayerTurn();
    }

    private void updateScoreOrSetGameOverIfKing(Piece piece) {
        if (isKing(piece)) {
            gameOver();
        } else {
            updateScore();
        }
    }

    private void changePlayerTurn() {
        isWhitePlayersTurn = !isWhitePlayersTurn;
    }

    private void updateScore() {
        if(isWhitePlayersTurn){
            whitePlayerScore++;
        } else {
            blackPlayerScore++;
        }
    }

    private void gameOver() {
        output.println(getColorOfPlayerWhoseTurn() + " player wins! Game over.");
        gameOver = true;
    }

    /**
     * @return  competitors piece if it is present on specified coordinate.
     * Or empty if piece of the same color located on this point or point is empty.
     */
    private Optional<Piece> takeEnemyPiece(Coordinate coordinate) {
        Optional<Piece> piece = board.getPieceFor(coordinate);
        if (piece.isPresent() && pieceHasDifferentColor(piece.get())) {
            return piece;
        } else {
            return empty();
        }
    }

    private boolean isKing(Piece piece) {
        return piece.getClass().equals(King.class);
    }

    private Move getValidUserMove() {
        Move userMove = inputController.getUserMove();
        Optional<String> errorMessage = validateMove(userMove);
        if(errorMessage.isPresent()) {
            errorOutput.println(errorMessage.get());
            return getValidUserMove();
        } else {
            return userMove;
        }
    }

    /**
     * @return returns error message or empty if valid
     */
    private Optional<String> validateMove(Move userMove) {
        Optional<Piece> piece = board.getPieceFor(userMove.getFrom());
        if(piece.isPresent()) {
            return validateMoveOfPiece(userMove, piece.get());
        } else {
            return of("Piece is absent at " + convertToUserPresentation(userMove.getFrom()));
        }
    }

    /**
     * @return returns error message or empty if valid
     */
    private Optional<String> validateMoveOfPiece(Move userMove, Piece piece) {
        if (pieceHasDifferentColor(piece)) {
            return of("Please choose piece of your color to move");
        } else if (piece.isMoveValid(userMove.getTo(), board)) {
            return empty();
        } else {
            return of("Move is invalid");
        }
    }

    private boolean pieceHasDifferentColor(Piece piece) {
        return piece.isWhite() != isWhitePlayersTurn;
    }

    private void printWhoseTurn() {
        output.println(getColorOfPlayerWhoseTurn() + "'s player turn to move");
        output.println(LINE_BORDER);
    }

    private void printScore() {
        output.println("| Score: White " + whitePlayerScore + " | Black " + blackPlayerScore + " |");
        output.println(LINE_BORDER);
    }

    private String getColorOfPlayerWhoseTurn() {
        return isWhitePlayersTurn ? WHITE_PLAYER : BLACK_PLAYER;
    }

}
