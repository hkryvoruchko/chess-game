package com.hkryvoruchko.game.chess.board;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.hkryvoruchko.game.chess.coordinate.Coordinate;
import com.hkryvoruchko.game.chess.coordinate.Move;
import com.hkryvoruchko.game.chess.piece.King;
import com.hkryvoruchko.game.chess.piece.Queen;

import org.junit.Test;

public class BoardTest {

    private Board board = new Board();

    @Test
    public void shouldGetNoPieceIfItAbsentOnSpecifiedCoordinate() {
        assertFalse(board.getPieceFor(new Coordinate(0, 0)).isPresent());
    }

    @Test
    public void shouldPutAndGetPieceOnSpecifiedCoordinate() {
        Coordinate coordinate = new Coordinate(0, 0);
        Queen queen = new Queen(true);
        board.put(coordinate, queen);
        assertTrue(board.isCellNotEmpty(coordinate));
        assertEquals(queen, board.getPieceFor(coordinate).get());
        assertEquals(coordinate, queen.getCurrentCoordinate());
    }

    @Test
    public void shouldCellBeEmptyIfNoPieceOnSpecifiedCoordinate() {
        assertTrue(board.isCellEmpty(new Coordinate(0, 0)));
    }

    @Test
    public void shouldCellBeNotEmptyIfPiecePresentOnSpecifiedCoordinate() {
        Coordinate coordinate = new Coordinate(0, 0);
        board.put(coordinate, new King(true));
        assertTrue(board.isCellNotEmpty(new Coordinate(0, 0)));
    }

    @Test
    public void shouldMovePiece() {
        Coordinate initial = new Coordinate(0, 0);
        Coordinate destination = new Coordinate(1, 1);
        King king = new King(true);
        board.put(initial, king);

        board.makeMove(new Move(initial, destination));

        assertTrue(board.isCellEmpty(initial));
        assertEquals(king, board.getPieceFor(destination).get());
        assertEquals(destination, king.getCurrentCoordinate());
    }
}