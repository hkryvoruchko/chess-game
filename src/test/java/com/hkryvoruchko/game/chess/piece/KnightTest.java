package com.hkryvoruchko.game.chess.piece;

import static org.junit.Assert.*;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class KnightTest {

    private Knight knight;

    @Before
    public void setUp() {
        knight = new Knight(true);
        knight.setCurrentCoordinate(new Coordinate(5, 5));
    }

    @Test
    public void shouldNotBeAbleToMoveStraightForward() {
        assertFalse(knight.isMoveValid(new Coordinate(5, 7), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveObliquely() {
        assertFalse(knight.isMoveValid(new Coordinate(7, 7), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveAside() {
        assertFalse(knight.isMoveValid(new Coordinate(7, 5), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveOneRowAndTwoCellsForward() {
        assertTrue(knight.isMoveValid(new Coordinate(7, 6), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveTwoRowsAndOneCellForward() {
        assertTrue(knight.isMoveValid(new Coordinate(7, 6), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveToCellWhichHasPieceOfSameColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(6, 7);
        board.put(destination, new Queen(true));

        assertFalse(knight.isMoveValid(destination, board));
    }

    @Test
    public void shouldBeAbleToMoveToCellWhichHasPieceOfDifferentColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(6, 7);
        board.put(destination, new Queen(false));

        assertTrue(knight.isMoveValid(destination, board));
    }

}