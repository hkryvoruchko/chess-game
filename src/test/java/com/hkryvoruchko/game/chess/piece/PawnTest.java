package com.hkryvoruchko.game.chess.piece;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PawnTest {

    private static final int INITIAL_ROW_FOR_WHITE_PAWNS = 6;
    private static final int INITIAL_ROW_FOR_BLACK_PAWNS = 1;

    private Pawn whitePawn;
    private Pawn blackPawn;

    @Before
    public void setUp() {
        whitePawn = new Pawn(true, INITIAL_ROW_FOR_WHITE_PAWNS);
        whitePawn.setCurrentCoordinate(new Coordinate(0, 6));
        blackPawn = new Pawn(false, INITIAL_ROW_FOR_BLACK_PAWNS);
        blackPawn.setCurrentCoordinate(new Coordinate(0, 2));
    }

    @Test
    public void shouldBeAbleToMoveRowForward_pawnWhite() {
        assertTrue(whitePawn.isMoveValid(new Coordinate(0, 5), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveRowForward_pawnBlack() {
        assertTrue(blackPawn.isMoveValid(new Coordinate(0, 3), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveTwoRowsForwardFromInitialPosition_pawnWhite() {
        assertTrue(whitePawn.isMoveValid(new Coordinate(0, 4), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveTwoRowsForwardFromInitialPosition_pawnBlack() {
        blackPawn.setCurrentCoordinate(new Coordinate(0, 1));
        assertTrue(blackPawn.isMoveValid(new Coordinate(0, 3), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveTwoRowsForwardFromNotInitialPosition_pawnWhite() {
        whitePawn.setCurrentCoordinate(new Coordinate(0, 5));
        assertFalse(whitePawn.isMoveValid(new Coordinate(0, 3), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveTwoRowsForwardFromNotInitialPosition_pawnBlack() {
        assertFalse(blackPawn.isMoveValid(new Coordinate(0, 4), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveTwoRowsForwardFromInitialPosition_whenObstaclePresent_pawnWhite() {
                Board board = new Board();
        board.put(new Coordinate(0, 5), new Pawn(false, INITIAL_ROW_FOR_BLACK_PAWNS));
        assertFalse(whitePawn.isMoveValid(new Coordinate(0, 4), board));
    }

    @Test
    public void shouldNotBeAbleToMoveTwoRowsForwardFromInitialPosition_whenObstaclePresent_pawnBlack() {
        blackPawn.setCurrentCoordinate(new Coordinate(0, 1));
        Board board = new Board();
        board.put(new Coordinate(0, 2), new Pawn(false, INITIAL_ROW_FOR_BLACK_PAWNS));
        assertFalse(blackPawn.isMoveValid(new Coordinate(0, 3), board));
    }

    @Test
    public void shouldBeAbleToMoveObliquelyForward_whenDestinationHasEnemyPiece_pawnWhite() {
        Board board = new Board();
        board.put(new Coordinate(1, 5), new Pawn(false, INITIAL_ROW_FOR_BLACK_PAWNS));
        assertTrue(whitePawn.isMoveValid(new Coordinate(1, 5), board));
    }

    @Test
    public void shouldBeAbleToMoveObliquelyForward_whenDestinationHasEnemyPiece_pawnBlack() {
        Board board = new Board();
        board.put(new Coordinate(1, 3), new Pawn(true, INITIAL_ROW_FOR_WHITE_PAWNS));
        assertTrue(blackPawn.isMoveValid(new Coordinate(1, 3), board));
    }

    @Test
    public void shouldNotBeAbleToMoveObliquelyForward_whenDestinationHasPieceOfSameColor_pawnWhite() {
        Board board = new Board();
        board.put(new Coordinate(1, 5), new Pawn(true, INITIAL_ROW_FOR_WHITE_PAWNS));
        assertFalse(whitePawn.isMoveValid(new Coordinate(1, 5), board));
    }

    @Test
    public void shouldNotBeAbleToMoveObliquelyForward_whenDestinationHasPieceOfSameColor_pawnBlack() {
        Board board = new Board();
        board.put(new Coordinate(1, 3), new Pawn(false, INITIAL_ROW_FOR_BLACK_PAWNS));
        assertFalse(blackPawn.isMoveValid(new Coordinate(1, 3), board));
    }
}