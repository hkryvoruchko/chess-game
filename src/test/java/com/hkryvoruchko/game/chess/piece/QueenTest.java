package com.hkryvoruchko.game.chess.piece;

import static org.junit.Assert.*;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class QueenTest {

    private Queen queen;

    @Before
    public void setUp() {
        queen = new Queen(true);
        queen.setCurrentCoordinate(new Coordinate(5, 5));
    }

    @Test
    public void shouldBeAbleToMoveStraightForward() {
        assertTrue(queen.isMoveValid(new Coordinate(5, 7), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveObliquely() {
        assertTrue(queen.isMoveValid(new Coordinate(7, 7), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveAside() {
        assertTrue(queen.isMoveValid(new Coordinate(7, 5), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveOneRowAndTwoCellsForward() {
        assertFalse(queen.isMoveValid(new Coordinate(7, 6), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveToCellWhichHasPieceOfSameColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(7, 7);
        board.put(destination, new Bishop(true));

        assertFalse(queen.isMoveValid(destination, board));
    }

    @Test
    public void shouldBeAbleToMoveToCellWhichHasPieceOfDifferentColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(7, 7);
        board.put(destination, new Bishop(false));

        assertTrue(queen.isMoveValid(destination, board));
    }

    @Test
    public void shouldNotBeAbleToMoveIfObstaclePresentOnObliqueWay() {
        Board board = new Board();
        board.put(new Coordinate(6, 6), new Bishop(false));

        assertFalse(queen.isMoveValid(new Coordinate(7, 7), board));
    }

    @Test
    public void shouldNotBeAbleToMoveIfObstaclePresentOnStraightWay() {
        Board board = new Board();
        board.put(new Coordinate(5, 6), new Queen(false));

        assertFalse(queen.isMoveValid(new Coordinate(5, 7), board));
    }


}