package com.hkryvoruchko.game.chess.piece;

import static org.junit.Assert.*;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class RookTest {

    private Rook rook;

    @Before
    public void setUp() {
        rook = new Rook(true);
        rook.setCurrentCoordinate(new Coordinate(5, 5));
    }

    @Test
    public void shouldBeAbleToMoveStraightForward() {
        assertTrue(rook.isMoveValid(new Coordinate(5, 7), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveObliquely() {
        assertFalse(rook.isMoveValid(new Coordinate(7, 7), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveAside() {
        assertTrue(rook.isMoveValid(new Coordinate(7, 5), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveToCellWhichHasPieceOfSameColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(5, 7);
        board.put(destination, new Queen(true));

        assertFalse(rook.isMoveValid(destination, board));
    }

    @Test
    public void shouldBeAbleToMoveToCellWhichHasPieceOfDifferentColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(5, 7);
        board.put(destination, new Queen(false));

        assertTrue(rook.isMoveValid(destination, board));
    }

    @Test
    public void shouldNotBeAbleToMoveIfObstaclePresent() {
        Board board = new Board();
        board.put(new Coordinate(5, 6), new Queen(false));

        assertFalse(rook.isMoveValid(new Coordinate(5, 7), board));
    }

}