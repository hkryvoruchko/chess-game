package com.hkryvoruchko.game.chess.piece;

import static org.junit.Assert.*;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class BishopTest {

    private Bishop bishop;

    @Before
    public void setUp() {
        bishop = new Bishop(true);
        bishop.setCurrentCoordinate(new Coordinate(5, 5));
    }

    @Test
    public void shouldNotBeAbleToMoveForward() {
        assertFalse(bishop.isMoveValid(new Coordinate(5, 7), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveObliquely() {
        assertTrue(bishop.isMoveValid(new Coordinate(7, 7), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveAside() {
        assertFalse(bishop.isMoveValid(new Coordinate(7, 5), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveToCellWhichHasPieceOfSameColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(7, 7);
        board.put(destination, new Queen(true));

        assertFalse(bishop.isMoveValid(destination, board));
    }

    @Test
    public void shouldBeAbleToMoveToCellWhichHasPieceOfDifferentColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(7, 7);
        board.put(destination, new Queen(false));

        assertTrue(bishop.isMoveValid(destination, board));
    }

    @Test
    public void shouldNotBeAbleToMoveIfObstaclePresent() {
        Board board = new Board();
        board.put(new Coordinate(6, 6), new Queen(false));

        assertFalse(bishop.isMoveValid(new Coordinate(7, 7), board));
    }


}