package com.hkryvoruchko.game.chess.piece;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.hkryvoruchko.game.chess.board.Board;
import com.hkryvoruchko.game.chess.coordinate.Coordinate;

import org.junit.Before;
import org.junit.Test;

public class KingTest {

    private King king;

    @Before
    public void setUp() {
        king = new King(true);
        king.setCurrentCoordinate(new Coordinate(5, 5));
    }

    @Test
    public void shouldBeAbleToMoveOneCellForward() {
        assertTrue(king.isMoveValid(new Coordinate(5, 6), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveOneCellObliquely() {
        assertTrue(king.isMoveValid(new Coordinate(6, 6), new Board()));
    }

    @Test
    public void shouldBeAbleToMoveOneCellAside() {
        assertTrue(king.isMoveValid(new Coordinate(6, 5), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveTwoCellsForward() {
        assertFalse(king.isMoveValid(new Coordinate(5, 7), new Board()));
    }

    @Test
    public void shouldNotBeAbleToMoveToCellWhichHasPieceOfSameColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(5, 6);
        board.put(destination, new Queen(true));

        assertFalse(king.isMoveValid(destination, board));
    }

    @Test
    public void shouldBeAbleToMoveToCellWhichHasPieceOfDifferentColor() {
        Board board = new Board();
        Coordinate destination = new Coordinate(5, 6);
        board.put(destination, new Queen(false));

        assertTrue(king.isMoveValid(destination, board));
    }
}