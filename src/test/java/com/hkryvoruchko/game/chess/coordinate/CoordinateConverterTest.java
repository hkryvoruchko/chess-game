package com.hkryvoruchko.game.chess.coordinate;

import org.junit.Test;

import static com.hkryvoruchko.game.chess.util.CoordinateConverter.*;
import static org.junit.Assert.assertEquals;

public class CoordinateConverterTest {

    @Test
    public void shouldConvertColumnFromUserPresentation() {
        assertEquals(0, convertColumnFromUserPresentation('a'));
        assertEquals(7, convertColumnFromUserPresentation('h'));
    }

    @Test
    public void shouldConvertRowFromUserPresentation() {
        assertEquals(7, convertRowFromUserPresentation('1'));
        assertEquals(0, convertRowFromUserPresentation('8'));
    }

    @Test
    public void shouldConvertCoordinateToUserPresentation() {
        assertEquals("a8", convertToUserPresentation(new Coordinate(0, 0)));
        assertEquals("h1", convertToUserPresentation(new Coordinate(7, 7)));
    }
}