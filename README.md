**This is a game of chess.**

The board is viewed in the console and is controlled with a string input, such as "d2 to d4". 
Player wins if takes opponent's king.

**Representation of pieces:**

Upper-case letters - black pieces. Lower-case letters - white pieces.
P - pawn,
R - rook,
N - knight,
B - bishop,
Q - queen,
K - king

**Running:**

Compile the project into an executable .jar file by running the following maven build script on the command line:
_mvn package_
Then, run the executable .jar file, named _chess-game-1.0-SNAPSHOT.jar_ to play.

Or just execute _startGame.bat_ for Windows.